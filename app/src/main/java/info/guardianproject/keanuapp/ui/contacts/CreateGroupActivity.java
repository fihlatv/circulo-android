/*
 * Copyright (C) 2008 Esmertec AG. Copyright (C) 2008 The Android Open Source
 * Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package info.guardianproject.keanuapp.ui.contacts;

import android.content.Intent;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.ListPopupWindow;

import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import info.guardianproject.keanu.core.model.ImConnection;
import info.guardianproject.keanu.core.provider.Imps;
import info.guardianproject.keanu.core.service.IImConnection;
import info.guardianproject.keanu.core.service.RemoteImService;
import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.BaseActivity;
import info.guardianproject.keanuapp.ui.legacy.SimpleAlertHandler;

import static info.guardianproject.keanu.core.KeanuConstants.LOG_TAG;


public class CreateGroupActivity extends BaseActivity {
    private static final String TAG = "AddContactActivity";

    private static final String[] CONTACT_LIST_PROJECTION = { Imps.ContactList._ID,
                                                             Imps.ContactList.NAME, };
    private static final int CONTACT_LIST_NAME_COLUMN = 1;

    private EditText mNewAddress;
    //private Spinner mListSpinner;
  //  Button mInviteButton;
    ImApp mApp;
    SimpleAlertHandler mHandler;

    private IImConnection mConn;
    private boolean mAddLocalContact = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        setTitle("");

        mApp = (ImApp)getApplication();

        long providerId = getIntent().getLongExtra(ContactsPickerActivity.EXTRA_RESULT_PROVIDER,mApp.getDefaultProviderId());
        long accountId = getIntent().getLongExtra(ContactsPickerActivity.EXTRA_RESULT_ACCOUNT,mApp.getDefaultAccountId());

        mConn = RemoteImService.getConnection(providerId, accountId);

        mHandler = new SimpleAlertHandler(this);

        setContentView(R.layout.create_group_activity);

        mNewAddress = (EditText) findViewById(R.id.tvGroupNameEdit);
        /**
        mNewAddress.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {

                    addNewGroup();
                }
                return false;
            }
        });**/

        if (getIntent().hasExtra("name"))
        {
            mNewAddress.setText(getIntent().getStringExtra("name"));
        }

        findViewById(R.id.btnCreateGroup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewGroup();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



    }


    @Override
    protected void onResume() {
        super.onResume();

        checkConnection();

    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        
    }





    private synchronized void addNewGroup() {


        String groupname = mNewAddress.getText().toString();

        if (!TextUtils.isEmpty(groupname)) {

            Intent data = new Intent();
            data.putExtra(ContactsPickerActivity.EXTRA_RESULT_USERNAME, groupname);

            setResult(RESULT_OK, data);

            finish();
        }



    }



    /**
    private String getSelectedListName() {
        Cursor c = (Cursor) mListSpinner.getSelectedItem();
        return (c == null) ? null : c.getString(CONTACT_LIST_NAME_COLUMN);
    }*/

    private View.OnClickListener mButtonHandler = new View.OnClickListener() {
        public void onClick(View v) {
            mApp.callWhenServiceConnected(mHandler, new Runnable() {
                public void run() {
                    addNewGroup();
                }
            });
        }
    };




    private static void log(String msg) {
        Log.d(LOG_TAG, "<AddContactActivity> " + msg);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    private boolean checkConnection() {
        try {
            if (mApp.getDefaultProviderId() != -1) {
                IImConnection conn = RemoteImService.getConnection(mApp.getDefaultProviderId(), mApp.getDefaultAccountId());

                if (conn.getState() == ImConnection.DISCONNECTED
                        || conn.getState() == ImConnection.SUSPENDED
                        || conn.getState() == ImConnection.SUSPENDING)
                    return false;
            }

            return true;
        } catch (Exception e) {
            return false;
        }

    }

}
