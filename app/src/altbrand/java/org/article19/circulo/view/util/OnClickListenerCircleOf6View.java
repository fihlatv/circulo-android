package org.article19.circulo.view.util;
/**
 * Created by Edgar Salvador Maurilio on 02/09/2015.
 */
public interface OnClickListenerCircleOf6View
{

    void contactClicked(int position);

    void phoneClicked();

    void locationClicked();

    void messageClicked();

    void informationClicked();

    void button911Clicked();

}
