package org.article19.circulo.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vanniktech.emoji.EmojiUtils;

import org.article19.circulo.dialog.QuickReplyDialog;
import org.article19.circulo.model.Contact;
import org.article19.circulo.model.ContactStatus;
import org.article19.circulo.model.ContactStatusReply;
import org.article19.circulo.model.ContactStatusUpdate;
import org.article19.circulo.view.StatusViewHolder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import info.guardianproject.keanu.core.model.ImErrorInfo;
import info.guardianproject.keanu.core.model.Message;
import info.guardianproject.keanu.core.provider.Imps;
import info.guardianproject.keanu.core.service.IChatListener;
import info.guardianproject.keanu.core.service.IChatSession;
import info.guardianproject.keanu.core.service.IChatSessionListener;
import info.guardianproject.keanu.core.service.IChatSessionManager;
import info.guardianproject.keanu.core.service.IImConnection;
import info.guardianproject.keanu.core.service.RemoteImService;
import info.guardianproject.keanu.core.util.LogCleaner;
import info.guardianproject.keanuapp.ImApp;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.conversation.ConversationListItem;
import info.guardianproject.keanuapp.ui.widgets.ConversationViewHolder;

import static info.guardianproject.keanu.core.KeanuConstants.LOG_TAG;

public class FragmentViewStatus extends Fragment {

    private ConversationListPagerAdapter mAdapter = null;
    private Uri mUri;
    private MyLoaderCallbacks mLoaderCallbacks;
    private LoaderManager mLoaderManager;
    private int LOADER_ID = 8001;
    private int mReplyLoaderId = 8002;
    private ViewPager statusPager;

    private int mChatId = -1;
    private String mRoomAddress = null;
    private ImApp mApp = null;
    private IImConnection mConn = null;
    private IChatSession mSess = null;

    private IChatSessionListener mListener = new IChatSessionListener() {
        @Override
        public void onChatSessionCreated(IChatSession session) throws RemoteException {

        }

        @Override
        public void onChatSessionCreateError(String name, ImErrorInfo error) throws RemoteException {

        }

        @Override
        public IBinder asBinder() {
            return null;
        }

    };

    private IChatListener mChatListener = new IChatListener() {
        @Override
        public boolean onIncomingMessage(IChatSession ses, Message msg) throws RemoteException {
            return false;
        }

        @Override
        public void onIncomingData(IChatSession ses, byte[] data) throws RemoteException {

        }

        @Override
        public void onSendMessageError(IChatSession ses, Message msg, ImErrorInfo error) throws RemoteException {

        }

        @Override
        public void onConvertedToGroupChat(IChatSession ses) throws RemoteException {

        }

        @Override
        public void onContactJoined(IChatSession ses, info.guardianproject.keanu.core.model.Contact contact) throws RemoteException {

        }

        @Override
        public void onContactLeft(IChatSession ses, info.guardianproject.keanu.core.model.Contact contact) throws RemoteException {

        }

        @Override
        public void onInviteError(IChatSession ses, ImErrorInfo error) throws RemoteException {

        }

        @Override
        public void onIncomingReceipt(IChatSession iChatSession, String s, boolean b) throws RemoteException {

        }


        @Override
        public void onStatusChanged(IChatSession ses) throws RemoteException {

        }

        @Override
        public void onIncomingFileTransfer(String from, String file) throws RemoteException {

        }

        @Override
        public void onIncomingFileTransferProgress(String file, int percent) throws RemoteException {

        }

        @Override
        public void onIncomingFileTransferError(String file, String message) throws RemoteException {

        }

        @Override
        public void onContactTyping(IChatSession ses, info.guardianproject.keanu.core.model.Contact contact, boolean isActive) throws RemoteException {

        }

        @Override
        public void onGroupSubjectChanged(IChatSession ses) throws RemoteException {

        }

        @Override
        public void onContactRoleChanged(IChatSession ses, info.guardianproject.keanu.core.model.Contact contact) throws RemoteException {

        }

        @Override
        public void onBeginMemberListUpdate(IChatSession ses) throws RemoteException {

        }

        @Override
        public void onEndMemberListUpdate(IChatSession ses) throws RemoteException {

        }

        @Override
        public IBinder asBinder() {
            return null;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.circulo_fragment_main_contacts, container, false);

        setupViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initAdapter();

        /**
        try {
            mConn.getChatSessionManager().registerChatSessionListener(mListener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }**/
    }

    @Override
    public void onPause() {
        super.onPause();

        /**
        try {
            mConn.getChatSessionManager().unregisterChatSessionListener(mListener);

            if (mSess != null)
            {
             //   mSess.unregisterChatListener(mChatListener);
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        }**/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (mApp == null) {
            mApp = (ImApp) getActivity().getApplication();
            mConn = RemoteImService.getConnection(mApp.getDefaultProviderId(), mApp.getDefaultAccountId());
            initAdapter();
        }

    }

    private void initAdapter () {

        Bundle args = getArguments();
        if (args == null)
            return;


        int newChatId = args.getInt("id", -1);

        mChatId = newChatId;
        mRoomAddress = args.getString("user",null);

        Uri baseUri = Imps.Messages.CONTENT_URI;//Imps.Messages.getContentUriByThreadId(mChatId);
        Uri.Builder builder = baseUri.buildUpon();
        mUri = builder.build();

        mAdapter = new ConversationListPagerAdapter(getActivity(), null);
        mAdapter.setOnReplyListener(mOnReplyListener);

        getChatSession(new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                try
                {
                    mSess = mConn.getChatSessionManager().getChatSession(mRoomAddress);

                }
                catch (RemoteException re)
                {
                    Log.e(getClass().getName(),"error inviting contact",re);
                }

                return null;
            }
        });

        if (statusPager != null)
            statusPager.setAdapter(mAdapter);

        if (mLoaderManager == null) {
            mLoaderManager = getLoaderManager();
        }
        else
        {
         //   mLoaderManager.destroyLoader(LOADER_ID);
        }

        mLoaderCallbacks = new MyLoaderCallbacks();
        mLoaderManager.initLoader(LOADER_ID, null, mLoaderCallbacks);

    }

    private StatusViewHolder.OnReplyListener mOnReplyListener = new StatusViewHolder.OnReplyListener() {
        @Override
        public void onReply(final Contact contact, String replyId, String message) {

            sendReply(contactYou, message, false, replyId);

        }

        @Override
        public void onQuickReply(final Contact contact,  String replyId, View anchorButton) {
            QuickReplyDialog.showFromAnchor(statusPager, anchorButton, new QuickReplyDialog.QuickReplyDialogListener() {
                @Override
                public void onQuickReplySelected(int emoji) {
                    onReply(contactYou, replyId, emoji);
                }
            });
        }

        @Override
        public void onReply(Contact contact, String replyId, int emoji) {

            ContactStatusReply reply = new ContactStatusReply();
            reply.setContact(contactYou);
            reply.setDate(new Date());
            reply.setType(ContactStatusReply.ReplyType.Emoji);
            reply.setEmoji(emoji);
            sendReply(contactYou, new String(Character.toChars(emoji)),false, replyId);
        }

        @Override
        public void onUnreply(Contact contact, int emoji) {
            /**
             for (ContactStatusReply reply : contact.getStatus().getReplyList()) {
             if (reply.getContact().isYou() && reply.getEmoji() == emoji) {
             sendReply(contact.getPhoneNumber(),""+((char)emoji));
             break;
             }
             }**/
        }

        @Override
        public void handleInvite(Contact contact) {

            String address = contact.getAddress();

            try {
                final IChatSession chatSession = mConn.getChatSessionManager().getChatSession(address);

                if (chatSession != null) {

                    //check if there is an open invite, if so, show the user a "join" dialog
                    //  showInviteDialog ();
                    boolean approveInvite = false;
                    if (approveInvite)
                    {
                        try {
                            chatSession.markAsSeen();
                            chatSession.setLastMessage(" ");
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        try {
                            chatSession.leave();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else
                {
                    startSessionForInvite(contact, mConn);
                }

            } catch (RemoteException e) {
                e.printStackTrace();
            }

        }
    };



    private void sendReply (Contact contact, String msg, boolean isEphemeral, String replyId)
    {

        ImApp mApp = (ImApp)getActivity().getApplication();
        IImConnection conn = RemoteImService.getConnection(mApp.getDefaultProviderId(),mApp.getDefaultAccountId());

        try {
            IChatSession session = conn.getChatSessionManager().getChatSession(mRoomAddress);
            if (session != null)
                session.sendMessage(msg,false, isEphemeral, false, replyId);
            else
                startSessionAndSend(mRoomAddress,conn,msg, isEphemeral);

            mAdapter.onContactStatusUpdated(contact);

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void startSessionAndSend (String contact, IImConnection conn, final String msgToSend, final boolean isEphemeral)
    {

        try {


            String roomSubject = getString(R.string.new_group_title);

            IChatSessionManager manager = conn.getChatSessionManager();

            String[] aInvitees = {contact};

            manager.createMultiUserChatSession(null, roomSubject, true, aInvitees, true, true, new IChatSessionListener() {

                @Override
                public IBinder asBinder() {
                    return null;
                }

                @Override
                public void onChatSessionCreated(final IChatSession session) throws RemoteException {

                    session.sendMessage(msgToSend, false, isEphemeral, false, null);

                }

                @Override
                public void onChatSessionCreateError(String name, ImErrorInfo error) throws RemoteException {


                    String errorMessage = getString(R.string.error);
                    if (error != null)
                        errorMessage = error.getDescription();

                }
            });


        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }



    public void startSessionForInvite (final Contact contact, IImConnection conn)
    {

        try {


            String roomSubject = getString(R.string.new_group_title);

            IChatSessionManager manager = conn.getChatSessionManager();

            String[] aInvitees = {contact.getAddress()};

            manager.createMultiUserChatSession(null, roomSubject, true, aInvitees, true, true, new IChatSessionListener() {

                @Override
                public IBinder asBinder() {
                    return null;
                }

                @Override
                public void onChatSessionCreated(final IChatSession session) throws RemoteException {

                   mAdapter.getOnReplyListener().handleInvite(contact);

                }

                @Override
                public void onChatSessionCreateError(String name, ImErrorInfo error) throws RemoteException {


                    String errorMessage = getString(R.string.error);
                    if (error != null)
                        errorMessage = error.getDescription();

                }
            });


        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public class ConversationListPagerAdapter
            extends PagerAdapter {

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;
        private Context mContext;
        private Cursor mCursor;
        private StatusViewHolder.OnReplyListener onReplyListener;

        /**
         * Stores a mapping of contact->StatusViewHolder, so we can refresh particular pages when a contact changes status.
         */
        private Map<Contact, StatusViewHolder> viewHolderMap;

        public ConversationListPagerAdapter(Context context, Cursor cursor) {
            super();
            mBackground = mTypedValue.resourceId;
            mContext = context;
            mCursor = cursor;
            viewHolderMap = new HashMap<>();

            if (mCursor != null)
                resolveColumnIndex(mCursor);

        }

        public long getItemId (int position)
        {
            mCursor.moveToPosition(position);
            long chatId = mCursor.getLong(ConversationListItem.COLUMN_CONTACT_ID);
            return chatId;
        }


        public ConversationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.conversation_view, parent, false);
            view.setBackgroundResource(mBackground);

            ConversationViewHolder viewHolder = (ConversationViewHolder)view.getTag();

            if (viewHolder == null) {
                viewHolder = new ConversationViewHolder(view);
                view.setTag(viewHolder);
            }

            return viewHolder;
        }

        public StatusViewHolder.OnReplyListener getOnReplyListener() {
            return onReplyListener;
        }

        public void setOnReplyListener(StatusViewHolder.OnReplyListener onReplyListener) {
            this.onReplyListener = onReplyListener;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            StatusViewHolder holder = (StatusViewHolder)object;
            return -1;//contacts.indexOf(holder.contact);
        }

        @Override
        public int getCount() {
            if (mCursor != null)
                return mCursor.getCount();
            else
                return -1;
        }

        public void swapCursor (Cursor cursor)
        {
        //    if (mCursor != null && (!mCursor.isClosed()))
          //      mCursor.close();

            mCursor = cursor;

            if (mCursor != null)
             resolveColumnIndex(mCursor);
        }

        @Override
        public void startUpdate(@NonNull ViewGroup container) {
            super.startUpdate(container);
        }

        public Contact getContact (int position)
        {
            if (position >= mCursor.getCount() || (mCursor.isClosed()))
                return null;

            mCursor.moveToPosition(position);

            final long chatId = mCursor.getLong(mThreadIdColumn);
            final String address = mCursor.getString(mNicknameColumn).split("|")[1];

            if (TextUtils.isEmpty(address))
                return null;

            final String nickname = mCursor.getString(mNicknameColumn).split("|")[0];
            final String body = mCursor.getString(mBodyColumn);
            long lastMsgDate = mCursor.getLong(mDateColumn);
            final String packetId =  mCursor.getString(mPacketIdColumn);

            final Contact contact = new Contact(chatId,nickname,address);

            ContactStatus cStatus = new ContactStatus();
            ContactStatusUpdate update = new ContactStatusUpdate();
            update.setMessage(body);
            update.setDate(new Date(lastMsgDate));
            update.setSeen(false);
            update.setMessageId(packetId);
            cStatus.addUpdate(update);



            contact.setStatus(cStatus);
            contact.setPosition(position);

            return contact;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            StatusViewHolder holder = null;
            View view = LayoutInflater.from(container.getContext()).inflate(R.layout.status_page, container, false);
            holder = new StatusViewHolder(view);

            final Contact contact = getContact(position);

            if (contact != null) {
                holder.populateWithContact(contact);
                loadReplies (contact,holder);
                holder.setOnReplyListener(getOnReplyListener());
                viewHolderMap.put(contact, holder);
            }

            container.addView(view);
            return holder;
        }

        private void loadReplies (Contact contact, StatusViewHolder holder)
        {

            String messageId =contact.getStatus().getLatestUpdate(false).getMessageId();
       //     FragmentMain.this.mLoaderManager.destroyLoader(mReplyLoaderId);
            mReplyLoaderId++;
            StatusLoaderCallbacks loaderCallbacks = new StatusLoaderCallbacks(holder,contact,messageId);
            FragmentViewStatus.this.mLoaderManager.initLoader(mReplyLoaderId, null, loaderCallbacks);

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            StatusViewHolder holder = (StatusViewHolder)object;
            if (holder != null) {
                container.removeView(holder.itemView);
                viewHolderMap.remove(holder.contact);
            }
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return ((StatusViewHolder) object).itemView == view;
        }

        public void onContactStatusUpdated(Contact contact) {
            StatusViewHolder holder = viewHolderMap.get(contact);
            if (holder != null) {

                loadReplies(contact, holder);
                // Refresh
                holder.refresh();
            }


        }

        class StatusLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

            private Contact mContact = null;
            private String mStatusPacketId = null;
            private StatusViewHolder mHolder;

            public final String[] MESSAGE_PROJECTION = {
                    Imps.Messages._ID,
                    Imps.Messages.NICKNAME,
                    Imps.Messages.BODY,
                    Imps.Messages.TYPE,
                    Imps.Messages.IS_DELIVERED,
                    Imps.Messages.MIME_TYPE,
                    Imps.Messages.THREAD_ID,
                    Imps.Messages.REPLY_ID,
                    Imps.Messages.DATE,
                    Imps.Messages.PACKET_ID
            };

            public StatusLoaderCallbacks (StatusViewHolder holder, Contact contact, String statusPacketId)
            {
                super();

                mContact = contact;
                mStatusPacketId = statusPacketId;
                mHolder = holder;
            }


            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                StringBuilder buf = new StringBuilder();
                buf.append(Imps.Messages.REPLY_ID).append("=").append("\"").append(mStatusPacketId).append("\"");
                CursorLoader loader = null;

                loader = new CursorLoader(getActivity(), mUri, MESSAGE_PROJECTION,
                        buf == null ? null : buf.toString(), null, Imps.Messages.REVERSE_SORT_ORDER);

                return loader;
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor newCursor) {

                if (newCursor == null)
                    return; // the app was quit or something while this was working

                int nicknameCol = newCursor.getColumnIndexOrThrow(Imps.Messages.NICKNAME);
                int bodyCol = newCursor.getColumnIndexOrThrow(Imps.Messages.BODY);
                int dateCol = newCursor.getColumnIndexOrThrow(Imps.Messages.DATE);

                ArrayList<ContactStatusReply> replyList = new ArrayList<>();

                while (newCursor.moveToNext())
                {
                    String body = newCursor.getString(bodyCol);
                    String address = newCursor.getString(nicknameCol);
                    String nick = null;

                    if (TextUtils.isEmpty(address))
                    {
                        address = ((ImApp)getActivity().getApplication()).getDefaultUsername();
                        nick = ((ImApp)getActivity().getApplication()).getDefaultNickname();
                    }
                    else
                    {
                        try {
                            info.guardianproject.keanu.core.model.Contact contact = mConn.getContactListManager().getContactByAddress(address);

                            if (contact != null)
                                nick = contact.getName();
                            else
                                nick = address.split("\\|")[0];

                        } catch (RemoteException e) {
                            e.printStackTrace();
                            nick = address.split("\\|")[0];
                        }

                    }

                    if (EmojiUtils.isOnlyEmojis(body))
                    {
                        ContactStatusReply csr = new ContactStatusReply();
                        csr.setEmoji(Character.codePointAt(body,0));
                        csr.setType(ContactStatusReply.ReplyType.Emoji);
                        csr.setDate(new Date(newCursor.getLong(dateCol)));



                        Contact contact = new Contact(mChatId,nick,address);
                        csr.setContact(contact);

                        replyList.add(csr);
                    }
                    else if (!TextUtils.isEmpty(body.trim()))
                    {
                        ContactStatusReply csr = new ContactStatusReply();
                        csr.setMessage(body);
                        csr.setType(ContactStatusReply.ReplyType.Message);
                        csr.setDate(new Date(newCursor.getLong(dateCol)));

                        Contact contact = new Contact(mChatId,nick,address);
                        csr.setContact(contact);

                        replyList.add(csr);
                    }
                }

                mContact.getStatus().setReplyList(replyList);

                mHolder.refresh();

                newCursor.setNotificationUri(getActivity().getContentResolver(), mUri);

            }

            @Override
            public void onLoaderReset(Loader<Cursor> loader) {

            }




        }

        public ArrayList<ContactStatusReply> getReplies(long chatId, String address) {

            ArrayList<ContactStatusReply> replyList = new ArrayList<>();

            Cursor c = mCursor;

            if (c != null && c.getColumnCount() >= 8) {

                try {

                    int nicknameCol = c.getColumnIndexOrThrow(Imps.Messages.NICKNAME);
                    int bodyCol = c.getColumnIndexOrThrow(Imps.Messages.BODY);
                    int dateCol = c.getColumnIndexOrThrow(Imps.Messages.DATE);

                    int MAX_REPLIES = 10;
                    int count = 0;

                    while (c.moveToNext() && count++<MAX_REPLIES)
                    {
                        String body = c.getString(bodyCol);
                        if (EmojiUtils.isOnlyEmojis(body))
                        {
                            ContactStatusReply csr = new ContactStatusReply();
                            csr.setEmoji(Character.codePointAt(body,0));
                            csr.setType(ContactStatusReply.ReplyType.Emoji);
                            csr.setDate(new Date(c.getLong(dateCol)));

                            String nick = c.getString(nicknameCol).split("\\|")[0];

                            Contact contact = new Contact(chatId,nick,address);
                            csr.setContact(contact);

                            replyList.add(csr);
                        }
                        else if (!TextUtils.isEmpty(body.trim()))
                        {
                            ContactStatusReply csr = new ContactStatusReply();
                            csr.setMessage(body);
                            csr.setType(ContactStatusReply.ReplyType.Message);
                            csr.setDate(new Date(c.getLong(dateCol)));

                            String nick = c.getString(nicknameCol).split("\\|")[0];

                            Contact contact = new Contact(chatId,nick,address);
                            csr.setContact(contact);

                            replyList.add(csr);
                        }
                    }




                } catch (IllegalStateException ise) {

                }
            }


            return replyList;


        }

        private int mNicknameColumn;
        private int mBodyColumn;
        private int mDateColumn;
        private int mTypeColumn;
        private int mDeliveredColumn;
        private int mMimeTypeColumn;
        private int mIdColumn;
        private int mPacketIdColumn;
        private int mThreadIdColumn;
        private int mReplyIdColumn;

        private void resolveColumnIndex(Cursor c) {
            mNicknameColumn = c.getColumnIndexOrThrow(Imps.Messages.NICKNAME);
            mBodyColumn = c.getColumnIndexOrThrow(Imps.Messages.BODY);
            mDateColumn = c.getColumnIndexOrThrow(Imps.Messages.DATE);
            mTypeColumn = c.getColumnIndexOrThrow(Imps.Messages.TYPE);
            mDeliveredColumn = c.getColumnIndexOrThrow(Imps.Messages.IS_DELIVERED);
            mMimeTypeColumn = c.getColumnIndexOrThrow(Imps.Messages.MIME_TYPE);
            mIdColumn = c.getColumnIndexOrThrow(Imps.Messages._ID);
            mPacketIdColumn = c.getColumnIndexOrThrow(Imps.Messages.PACKET_ID);
            mThreadIdColumn = c.getColumnIndexOrThrow(Imps.Messages.THREAD_ID);
            mReplyIdColumn =  c.getColumnIndexOrThrow(Imps.Messages.REPLY_ID);
        }

    }


    class MyLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

        private int mLastCount = 0;

        public final String[] MESSAGE_PROJECTION = {
                "MAX (" + Imps.Messages._ID + ") as _id",
                Imps.Messages.NICKNAME,
                Imps.Messages.BODY,
                Imps.Messages.TYPE,
                Imps.Messages.IS_DELIVERED,
                Imps.Messages.MIME_TYPE,
                Imps.Messages.THREAD_ID,
                Imps.Messages.REPLY_ID,
                "MAX (" + Imps.Messages.DATE + ") as " + Imps.Messages.DATE,
                "MAX (" + Imps.Messages.PACKET_ID + ") as " + Imps.Messages.PACKET_ID
        };

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            StringBuilder buf = new StringBuilder();
            buf.append(Imps.Messages.THREAD_ID).append("=").append(mChatId);
            buf.append(" AND ");
            buf.append(Imps.Messages.REPLY_ID).append(" IS NULL ");

            buf.append(" AND (");
            buf.append(Imps.Messages.TYPE).append("=").append(Imps.MessageType.INCOMING);
            buf.append(" OR ");
            buf.append(Imps.Messages.TYPE).append("=").append(Imps.MessageType.INCOMING_ENCRYPTED);
            buf.append(") AND ");
            buf.append(Imps.Messages.BODY).append(" IS NOT NULL) GROUP BY (").append(Imps.Messages.NICKNAME); //GROUP BY

            CursorLoader loader = null;

            loader = new CursorLoader(getActivity(), mUri, MESSAGE_PROJECTION,
                    buf == null ? null : buf.toString(), null, Imps.Messages.REVERSE_SORT_ORDER);

            return loader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor newCursor) {

            if (newCursor == null)
                return; // the app was quit or something while this was working


            mAdapter.swapCursor(newCursor);
            updatePager();

            mAdapter.notifyDataSetChanged();

            mLastCount = mContactList.size();

            /**
            mAdapter.swapCursor(newCursor);

            mLastCount = mAdapter.getCount();
            statusPagerIndicator.setNumberOfDots(mAdapter.getCount());
            statusPagerIndicator.setCurrentDot(0);

            mAdapter.notifyDataSetChanged();

            newCursor.setNotificationUri(getActivity().getContentResolver(), mUri);
             **/
        }


        ArrayList<Contact> mContactList;

        private void updatePager ()
        {

            mContactList = new ArrayList<>();

            for (int i = 0; i < mAdapter.getCount(); i++)
            {
                mContactList.add(mAdapter.getContact(i));
            }

        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

            mAdapter.swapCursor(null);

        }




    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    //----------------------------------------------------------------Init
    public void setupViews(View view) {

        statusPager = view.findViewById(R.id.statusPager);
        statusPager.setAdapter(mAdapter);

    }

    private Runnable setSeenRunnable = new Runnable() {
        @Override
        public void run() {
            if (statusPager != null && mAdapter != null) {
                //Contact currentContact = mAdapter.getContacts().get(statusPager.getCurrentItem());
               // CircleOf6Application.getInstance().setStatusSeen(currentContact);
            }
        }
    };

    private static Contact contactYou;

    public static Contact getYouContact (Activity activity)
    {
        if (contactYou == null) {
            ImApp app = (ImApp)activity.getApplication();
            contactYou = new Contact(0, app.getDefaultNickname(), app.getDefaultUsername());//CircleOf6Application.getInstance().getContactWithId(id);
            contactYou.setYou(true);
        }
        return contactYou;

    }


    private void showInviteUI ()
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        getChatSession(new AsyncTask() {
                            @Override
                            protected Object doInBackground(Object[] objects) {

                                getChatSession(new AsyncTask() {
                                    @Override
                                    protected Object doInBackground(Object[] objects) {

                                        if (mSess != null) {
                                            try {
                                                mSess.markAsSeen();
                                            } catch (RemoteException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(Object o) {
                                        super.onPostExecute(o);

                                    }
                                });


                                return null;
                            }

                            @Override
                            protected void onPostExecute(Object o) {
                                super.onPostExecute(o);

                            }
                        });
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        getChatSession(new AsyncTask() {
                            @Override
                            protected Object doInBackground(Object[] objects) {

                                if (mSess != null) {
                                    try {
                                        mSess.leave();

                                    }
                                    catch (RemoteException re){}
                                }

                                return null;
                            }

                            @Override
                            protected void onPostExecute(Object o) {
                                super.onPostExecute(o);

                            }
                        });
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.join_this_circle).setPositiveButton(R.string.yes, dialogClickListener)
                .setNegativeButton(R.string.no, dialogClickListener).show();
    }

    public String getRoomAddress () {
        return mRoomAddress;
    }

    public IChatSession getChatSession(AsyncTask task) {

        try {

            if (mSess != null) {
                if (task != null)
                    task.execute();
                return mSess;
            }
            else if (mConn != null)
            {
                IChatSessionManager sessionMgr = mConn.getChatSessionManager();
                if (sessionMgr != null) {

                    IChatSession session = sessionMgr.getChatSession(mRoomAddress);

                    if (session == null) {
                        sessionMgr.createChatSession(mRoomAddress, false, new IChatSessionListener() {
                            @Override
                            public void onChatSessionCreated(IChatSession session) throws RemoteException {
                                mSess = session;

                                if (task != null)
                                    task.execute();
                            }

                            @Override
                            public void onChatSessionCreateError(String name, ImErrorInfo error) throws RemoteException {

                            }

                            @Override
                            public IBinder asBinder() {
                                return null;
                            }
                        });
                    }
                    else
                    {
                        if (task != null)
                            task.execute();
                    }

                    return session;

                }
            }


        } catch (Exception e) {

            //mHandler.showServiceErrorAlert(e.getLocalizedMessage());
            LogCleaner.error(LOG_TAG, "error getting chat session", e);
        }

        return null;
    }
}
